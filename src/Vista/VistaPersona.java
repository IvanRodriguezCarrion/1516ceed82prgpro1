package Vista;

import java.util.Scanner;
import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import java.io.IOException;
import Modelo.Persona;
/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaPersona {
  
  public Persona tomaDatos () throws IOException {
  int idpersona = 0;
  String nombre, apellidos, dni, direccion, telefono;
    
  
  Scanner teclado = new Scanner(System.in);
                               
  System.out.println("Crear nueva Persona: \n================");
  
  boolean salir_idp = false; //Esta es la condición para que salga se salga del bucle while
        
    while (!salir_idp) { //Mientras no-salir, o sea, mientras salir sea falso ha de continuar en el bucle.

      try {

         System.out.println("Introduzca la ID de la persona");
         idpersona = teclado.nextInt();
         salir_idp = true;        

      }
      catch (InputMismatchException e) { //Hemos escogido capturar esta excepción pues es la relacionada con la clase Scanner
        System.out.println("No se ha introducido un valor numérico, vuelva a intentarlo. ");
        /**
         * Proceso para eliminar el INTRO que queda capturado dentro del buffer cuando se aplica el nextLine
         * y que de no quitarse provocaría la entrada del sistema en un bucle infinito.
         */
        teclado.nextLine(); 
      }
    }
  
   System.out.println("Introduzca el nombre");
   nombre = teclado.nextLine();
   
   System.out.println("Introduzca los apellidos");
   apellidos = teclado.nextLine();
   
   System.out.println("Introduzca el DNI");
   dni = teclado.nextLine();
   
   System.out.println("Introduzca la dirección");
   direccion = teclado.nextLine();
   
   System.out.println("Introduzca el telefono");
   telefono = teclado.nextLine();
   
   
   
   
   
  Persona persona = new Persona(idpersona, nombre, apellidos, dni, direccion, telefono);
  
  return persona; 
  }
  
}