package Vista;

import java.util.Scanner;
import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import java.io.IOException;
import Modelo.Padrino;
/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaPadrino {
  
  public static Padrino tomaDatos () throws IOException {
  int idpersona = 0;
  String nombre, apellidos, dni, direccion, telefono;
    
  
  Scanner teclado = new Scanner(System.in);
                               
  System.out.println("Crear nuevo padrino: \n================");
  
  boolean no_salir_idp = true; //Esta es la condición para que salga se salga del bucle while
        
    do {

      try {

         System.out.println("Introduzca la ID de la persona");
         idpersona = teclado.nextInt();
         no_salir_idp = false;        

      }
      catch (InputMismatchException e) { //Hemos escogido capturar esta excepción pues es la relacionada con la clase Scanner
        System.out.println("No se ha introducido un valor numérico, vuelva a intentarlo. ");
        /**
         * Proceso para eliminar el INTRO que queda capturado dentro del buffer cuando se aplica el nextLine
         * y que de no quitarse provocaría la entrada del sistema en un bucle infinito.
         */
        teclado.nextLine(); 
      }
    } while (no_salir_idp); //Mientras sair se
    
  teclado.nextLine(); // DESPUÉS DE CADA INSERCIÓN DE UN INT SE DEBE LIMPIAR EL BUFFER
  
   System.out.println("Introduzca el nombre");
   nombre = teclado.nextLine();
   
   System.out.println("Introduzca los apellidos");
   apellidos = teclado.nextLine();
   
   System.out.println("Introduzca el DNI");
   dni = teclado.nextLine();
   
   System.out.println("Introduzca la dirección");
   direccion = teclado.nextLine();
   
   System.out.println("Introduzca el telefono");
   telefono = teclado.nextLine();
   
  Padrino padrino = new Padrino(idpersona, nombre, apellidos, dni, direccion, telefono);
  
  return padrino; 
  }
  
  public static void muestraDatos(Padrino p) {
    System.out.println("Mostrando Datos: ");
    System.out.println("Identificador de la persona: "+p.getIdPersona());
    System.out.println("Identificador del padrino: "+p.getIdPadrino());
    System.out.println("Nombre: "+p.getNombre());
    System.out.println("Apellidos: "+p.getApellidos());
    System.out.println("DNI: "+p.getDNI());
    System.out.println("Dirección: "+p.getDireccion());
    System.out.println("Teléfono: "+p.getTelefono());
    

  }
  
}