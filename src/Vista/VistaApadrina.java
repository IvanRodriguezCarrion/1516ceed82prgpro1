package Vista;

import java.util.Scanner;
import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import java.io.IOException;
import Modelo.Apadrina;
import Modelo.Perro;
import Modelo.Padrino;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaApadrina  {
  
  private static int idapadrina;
  private static Padrino padrino;
  private static Perro perro;
  
  public static Apadrina crearApadrina (Padrino pad, Perro per) throws IOException {
   
    
  Scanner teclado = new Scanner(System.in);
                               
  System.out.println("Crear nuevo apadrinamiento: \n================");
  
  boolean salir_id1 = false; //Esta es la condición para que salga se salga del bucle while
        
    while (!salir_id1) { //Mientras no-salir, o sea, mientras salir sea falso ha de continuar en el bucle.

      try {

         System.out.println("Introduzca la ID del apadrinamiento");
         idapadrina = teclado.nextInt();
         salir_id1 = true;        

      }
      catch (InputMismatchException e) { //Hemos escogido capturar esta excepción pues es la relacionada con la clase Scanner
        System.out.println("No se ha introducido un valor numérico, vuelva a intentarlo. ");
        /**
         * Proceso para eliminar el INTRO que queda capturado dentro del buffer cuando se aplica el nextLine
         * y que de no quitarse provocaría la entrada del sistema en un bucle infinito.
         */
        teclado.nextLine(); 
      }
    }
    
    padrino = pad;
    perro = per; 
    
    Apadrina apadrina = new Apadrina (idapadrina, padrino, perro);
    
    return apadrina; 
  }
  
  public static void muestraApadrina(Apadrina apadrina) {
    
//    Perro perro = new Perro();
//    Padrino padrino = new Padrino();
    VistaPerro vperro = new VistaPerro();
    VistaPadrino vpadrino = new VistaPadrino();
    
    
    System.out.println("Datos del apadrinamiento: ");
    System.out.println("Identificador del apadrinamiento: "+idapadrina);
    System.out.println("=======================");
    vpadrino.muestraDatos(apadrina.getPadrino());
//    System.out.println("Identificador: "+ padrino.getIdPadrino()+" Nombre: "+padrino.getNombre()+" Apellidos:" + padrino.getApellidos());
//    System.out.println("DNI: "+padrino.getDNI()+" Dirección: "+padrino.getDireccion()+" Teléfono: "+padrino.getTelefono());
    System.out.println("=======================");
    vperro.muestraDatosPerro(apadrina.getPerro());
//    System.out.println("Identificador: "+ perro.getIdPerro()+ " Nombre: "+perro.getNombre());
//    System.out.println("Número de chip: "+perro.getNChip()+" Raza: "+ perro.getRaza());
  
  }
  
  
//  public static void muestraDatos(Apadrina p) {
//    System.out.println("Mostrando Datos: ");
//    System.out.println(p.getNombre() + " " + p.getEdad());
//
//  }
  
}