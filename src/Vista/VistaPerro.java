package Vista;

import java.util.Scanner;
import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import java.io.IOException;
import Modelo.Perro;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaPerro {
  
  public static Perro tomaDatos () throws IOException {
  int idperro = 0, nchip = 0;
  String nombre, raza;
  
  Scanner teclado = new Scanner(System.in);
                               
  System.out.println("Crear nuevo perro: \n================");
  
  boolean salir_id = false; //Esta es la condición para que salga se salga del bucle while
        
    while (!salir_id) { //Mientras no-salir, o sea, mientras salir sea falso ha de continuar en el bucle.

      try {

         System.out.println("Introduzca la ID del perro");
         idperro = teclado.nextInt();
         salir_id = true;        

      }
      catch (InputMismatchException e) { //Hemos escogido capturar esta excepción pues es la relacionada con la clase Scanner
        System.out.println("No se ha introducido un valor numérico, vuelva a intentarlo. ");
        /**
         * Proceso para eliminar el INTRO que queda capturado dentro del buffer cuando se aplica el nextLine
         * y que de no quitarse provocaría la entrada del sistema en un bucle infinito.
         */
        teclado.nextLine(); 
      }
    }
  
  teclado.nextLine(); // DESPUÉS DE CADA INSERCIÓN DE UN INT SE DEBE LIMPIAR EL BUFFER
    
  System.out.println("Introduzca su nombre:");
  nombre = teclado.nextLine();
  
   boolean salir_nombre = false; //Esta es la condición para que salga se salga del bucle while
  
  while (!salir_nombre) { //Mientras no-salir, o sea, mientras salir sea falso ha de continuar en el bucle.

      try {

         System.out.println("Introduzca el número de chip");
         nchip = teclado.nextInt();
         salir_nombre = true;        

      }
      catch (InputMismatchException e) { //Hemos escogido capturar esta excepción pues es la relacionada con la clase Scanner
        System.out.println("No se ha introducido un valor numérico, vuelva a intentarlo. ");
        /**
         * Proceso para eliminar el INTRO que queda capturado dentro del buffer cuando se aplica el nextLine
         * y que de no quitarse provocaría la entrada del sistema en un bucle infinito.
         */
        teclado.nextLine(); 
      }
    }
  teclado.nextLine(); // DESPUÉS DE CADA INSERCIÓN DE UN INT SE DEBE LIMPIAR EL BUFFER
  
  System.out.println("Introduzca su raza:");
  raza = teclado.nextLine();
  
  
  
  Perro perro = new Perro(idperro, nombre, nchip, raza);
  
  return perro; 
  }
  
  public static void muestraDatosPerro(Perro perro) {
    System.out.println("Mostrando Datos del Perro: ");
    System.out.println("Identificador del perro: "+perro.getIdPerro());
    System.out.println("Nombre: "+perro.getNombre());
    System.out.println("Número de Chip: "+perro.getNChip());
    System.out.println("Raza: "+perro.getRaza());
  }
}