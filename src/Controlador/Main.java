package Controlador;
import java.io.IOException;
import Modelo.Padrino;
import Modelo.Perro;
import Modelo.Apadrina;
import Vista.VistaPadrino;
import Vista.VistaPerro;
import Vista.VistaApadrina;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Main {
  
  public static void main (String[] args) throws IOException {
   
   /**
    * La clase Main se encargará de controlar la ejecución
    * de los procesos del programa importando las clases
    * de los paquetes modelo y vista.
   */ 
    
    Padrino padrino = VistaPadrino.tomaDatos();
    Perro perro = VistaPerro.tomaDatos();
    Apadrina apadrina = VistaApadrina.crearApadrina(padrino, perro);
     
     VistaApadrina.muestraApadrina(apadrina);
     
//        Padrino p;
//        Vista
//        p = new Padrino();
//        vistapadrino = new VistaPadrino();
         
        /**
         * Le pasamos los datos de la vista al objeto p de Alumno
         * y los mostramos.
         */
        
//        p = vistapadrino.tomaDatos();
//        vistapadrino.muestraDatos(p);
//        
            
        
    }
}  